# Classic epidemic SIR model

An example of how to use modelbase v1.2.3 for epidemiologic.

We construct here the classic epidemic SIR (Susceptible, Infectious, Recovered populations) model by Kermack and McKendrick (1927). doi:10.1098/rspa.1927.0118

Later to show phase plotting options within the modelbase, we parametrised it to show oscilations as in the model that simulates periodicity of chicken pox outbreak in Hida, Japan, published in recently by Greer et al. in the Royal Society Open Science 7(1), 191187 (2020). [https://doi.org/10.1098/rsos.191187](https://doi.org/10.1098/rsos.191187)